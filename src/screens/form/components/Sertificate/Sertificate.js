import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import PropTypes from 'prop-types';
import { FormCheck } from "react-bootstrap";
import { Collapse } from '@material-ui/core';
import { TextField, SelectedField } from "../../../../components";
import { regNumber as regNumberValidate, dateFormat, countryCity } from '../../../../utils/maskValidation';
import { useDispatch, useSelector } from "react-redux";
import { setFormState } from "../../../../redux/actions/form";

const useStyles = makeStyles({
    root: {},
    header: {
        fontSize: 20,
        fontWeight: 'bold'
    },
});

Sertificate.defaultProps = {
    label: ''
}

Sertificate.propTypes = {
    label: PropTypes.string
}

export default function Sertificate({ label, onChange, errors }) {
    const classes = useStyles();
    const [checked, setChecked] = useState(false);

    const handleChecked = () => {
        setChecked(!checked);

        if (checked) {
            onChange(false);
        }
    };

    const [regNumber, setRegNumber] = useState('');
    const handleRegNumberChange = (e) => setRegNumber(e.target.value);

    const [create, setCreate] = useState('');
    const handleCreateChange = (e) => setCreate(e.target.value);

    const [end, setEnd] = useState('');
    const handleEndChange = (e) => setEnd(e.target.value);

    const [selected, setSelected] = useState([]);
    const handleActivity = selected => setSelected(selected);


    useEffect(() => {
        if (onChange) {
            onChange({regNumber, create, end, selected})
        }

    }, [regNumber, create, end, selected]);

    return (
        <div className={classes.root}>
            <FormCheck label={ label }
                       checked={ checked }
                       onClick={ handleChecked }/>

            <Collapse in={ checked }>
                <TextField label={ 'Регистрационный номер свидетельства' }/>

                <TextField label={ 'Регистрационный номер свидетельства' }
                           name={ 'reg_number_certificate' }
                           validation={ regNumberValidate }
                           onChange={ handleRegNumberChange }
                           value={ regNumber }
                           error={ false }/>

                <TextField label={ 'Дата выдачи свидетельства' }
                           name={ 'create_certificate' }
                           validation={ dateFormat }
                           onChange={ handleCreateChange }
                           value={ create }
                           error={ false }
                           helperText={ dateFormat.text }/>

                <TextField label={ 'Действителен до' }
                           name={ 'end_certificate' }
                           validation={ dateFormat.text }
                           onChange={ handleEndChange }
                           value={ end }
                           error={ false }
                           helperText={ dateFormat.text }/>

                <SelectedField label={'Область деятельности (ОК 034-2014)'}
                               selected={selected}
                               onChange={handleActivity} />
            </Collapse>
        </div>
    )
}