import React, { useRef, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Button, FormCheck } from 'react-bootstrap';
import { ExpandFormFields, TextField, FormValidation } from "../../components";
import { normalRus, email, phone, site, fullName, cityCode, countryCity } from "../../utils/maskValidation";
import { useDispatch, useSelector } from "react-redux";
import { setFormState } from "../../redux/actions/form";
import { Collapse } from "@material-ui/core";
import { Sertificate } from "./components";

const useStyles = makeStyles(() => ({
    root: {
        padding: 20
    },
    header: {
        fontSize: 20,
        fontWeight: 'bold'
    },
}))

export default function Form() {
    const classes = useStyles();
    const [errors, setErrors] = useState({});

    const createRef = function () {
        const ref = useRef();
        return ref;
    }

    const clearFieldInErrors = name => {
        const err = {};

        Object.keys(errors).forEach(key => {
            if (key !== name) {
                err[key] = errors[key];
            }
        })

        setErrors(err);
    }

    const dispatch = useDispatch();

    const stateForm = useSelector(store => store.form);

    const handleChangeField = (name, event, mask) => {
        const v = {};

        v[name] = mask ? mask(event.target.value) : event.target.value;

        clearFieldInErrors(name);

        dispatch(
            setFormState(v)
        )
    }

    const onSubmit = (data) => {
        const errorsFields = data.errors.filter(e => e.isError);
        const isHaveError = errorsFields.length !== 0;

        const err = {};

        errorsFields.forEach(field => {
            err[field.name] = true;
        });

        setErrors(err);

        console.log(errorsFields);


        if (!isHaveError) {

        }

    }

    const [isFactiv, setFactiv] = useState(false);

    const handleCheckFactiv = e => setFactiv(!isFactiv);

    const [isPresentSystem, setPresentSystem] = useState(false);

    const handleRadioPresentSystem = value => setPresentSystem(value);

    const handleCertificate= (name, certificate) => {

    }

    return (
        <div className={ classes.root }>
            <FormValidation onSubmit={ onSubmit }>

                <ExpandFormFields>
                    <TextField label={ 'Полное название испытательной лаборатории (центра)' }
                               name={ 'lab_name' }
                               validation={ normalRus }
                               onChange={ e => handleChangeField('lab_name', e) }
                               value={ stateForm.lab_name }
                               error={ Boolean(errors['lab_name']) }
                               helperText={ normalRus.text }/>

                    <TextField label={ 'Сокращенное название испытательной лаборатории (центра)' }
                               name={ 'short_lab_name' }
                               validation={ normalRus }
                               onChange={ e => handleChangeField('short_lab_name', e) }
                               value={ stateForm.short_lab_name }
                               error={ Boolean(errors['short_lab_name']) }
                               helperText={ normalRus.text }/>

                    <TextField label={ 'Адрес электронной почты (общий)' }
                               name={ 'email' }
                               validation={ email }
                               onChange={ e => handleChangeField('email', e) }
                               value={ stateForm.email }
                               error={ Boolean(errors['email']) }
                               helperText={ email.text }/>

                    <TextField label={ 'Телефон' }
                               name={ 'phone' }
                               validation={ phone }
                               onChange={ e => handleChangeField('phone', e, phone.mask) }
                               value={ stateForm.phone }
                               error={ Boolean(errors['phone']) }
                               helperText={ phone.text }/>

                    <TextField label={ 'Сайт' }
                               name={ 'site' }
                               validation={ site }
                               onChange={ e => handleChangeField('site', e) }
                               value={ stateForm.site }
                               error={ Boolean(errors['site']) }
                               helperText={ site.text }/>

                </ExpandFormFields>

                <ExpandFormFields header={ 'Руководитель' }>

                    <TextField label={ 'Фамилия' }
                               name={ 'full_name' }
                               validation={ fullName }
                               onChange={ e => handleChangeField('full_name', e) }
                               value={ stateForm.full_name }
                               error={ Boolean(errors['full_name']) }
                               helperText={ fullName.text }/>

                    <TextField label={ 'Имя' }
                               name={ 'first_name' }
                               validation={ fullName }
                               onChange={ e => handleChangeField('first_name', e) }
                               value={ stateForm.first_name }
                               error={ Boolean(errors['first_name']) }
                               helperText={ fullName.text }/>

                    <TextField label={ 'Отчество' }
                               name={ 'second_name' }
                               validation={ fullName }
                               onChange={ e => handleChangeField('second_name', e) }
                               value={ stateForm.second_name }
                               error={ Boolean(errors['second_name']) }
                               helperText={ fullName.text }/>

                    <TextField label={ 'Электронный адрес' }
                               name={ 'persona_email' }
                               validation={ email }
                               onChange={ e => handleChangeField('persona_email', e) }
                               value={ stateForm.persona_email }
                               error={ Boolean(errors['persona_email']) }
                               helperText={ email.text }/>

                    <TextField label={ 'Телефон' }
                               name={ 'persona_phone' }
                               validation={ phone }
                               onChange={ e => handleChangeField('persona_phone', e, phone.mask) }
                               value={ stateForm.persona_phone }
                               error={ Boolean(errors['persona_phone']) }
                               helperText={ phone.text }/>

                </ExpandFormFields>

                <ExpandFormFields header={ 'Фактический адрес' }>

                    <FormCheck label={ 'Совпадает с фактическим адресом организации' }
                               checked={ isFactiv }
                               onClick={ handleCheckFactiv }/>

                    <Collapse in={ !isFactiv }>

                        <TextField label={ 'Страна' }
                                   name={ 'country' }
                                   validation={ countryCity }
                                   onChange={ e => handleChangeField('country') }
                                   value={ stateForm.country }
                                   error={ Boolean(errors['country']) }
                                   helperText={ countryCity.text }/>

                        <TextField label={ 'Почтовый индекс' }
                                   name={ 'city_code' }
                                   validation={ cityCode }
                                   onChange={ e => handleChangeField('city_code') }
                                   value={ stateForm.city_code }
                                   error={ Boolean(errors['city_code']) }
                                   helperText={ cityCode.text }/>

                        <TextField label={ `Регион/область` }
                                   name={ 'region' }
                                   validation={ countryCity }
                                   onChange={ e => handleChangeField('region') }
                                   value={ stateForm.region }
                                   error={ Boolean(errors['region']) }
                                   helperText={ countryCity.text }/>

                        <TextField label={ `Город/район` }
                                   name={ 'city' }
                                   validation={ countryCity }
                                   onChange={ e => handleChangeField('city') }
                                   value={ stateForm.city }
                                   error={ Boolean(errors['city']) }
                                   helperText={ countryCity.text }/>

                    </Collapse>

                </ExpandFormFields>

                <span className={ classes.header }>Признание в системе</span>

                <FormCheck label={ 'Есть' }
                           checked={ isPresentSystem }
                           type={ 'radio' }
                           onClick={ e => handleRadioPresentSystem(true) }/>

                <FormCheck label={ 'Нет' }
                           type={ 'radio' }
                           checked={ !isPresentSystem }
                           onClick={ e => handleRadioPresentSystem(false) }/>

                <Collapse in={ isPresentSystem }>
                    <span className={ classes.header }>Центральный орган системы выдавший свидетельство</span>

                    <Sertificate label={"Системы менеджента"}
                                 errors={errors}
                                 onChange={v => handleCertificate('managmend', v)} />
                </Collapse>


                <Button type={ 'submit' }>
                    Далее
                </Button>
            </FormValidation>
        </div>
    )
}