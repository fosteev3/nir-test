import React, { Suspense } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import Form from "./form";

export default function Screens() {
    return (
        <BrowserRouter>
            <main>
                <div>
                    <Suspense fallback={<div>loading</div>}>
                        <Switch>
                            <Route path={'/form'} component={Form}></Route>
                            <Redirect from={'/'} to={'/form'}></Redirect>
                        </Switch>
                    </Suspense>
                </div>
            </main>
        </BrowserRouter>
    )
}