const mask = (v, mask) => {
    let numbers = String(v).replace(/[^0-9]/g, '').split('');

    if (['-', '+', '(', ')'].includes(phone[phone.length - 1])) {
        numbers.pop();
    }

    let str = '';

    let exNums = 0;

    const tmps = mask.split('');

    for (let i = 0; i < tmps.length; i++) {
        const symbol = tmps[i];

        if (symbol === 'x') {
            const num = numbers[exNums];

            if (!num) {
                break;
            }

            str += numbers[exNums];
            exNums++;
        } else {
            str += symbol;
        }
    }

    return str;
}


export const normalRus = {
    rule: /[А-Яа-я.,\s-"0-9]/g,
    text: 'Допускаются русские буквы, цифры, символы (.,()""-) и пробелы'
}

export const email = {
    rule: /[A-z.@-_0-9]/g,
    isValidate: (email) => {
        if (typeof email === 'undefined') {
            return false;
        }

        if (email.search('@') === -1) {
            return false;
        }

        const v = email.split('@')[0];
        const [domain, dot] = email.split('@')[1].split('.');
        return v && domain && dot;
    },
    text: 'Допускаются латинские буквы, цифры и символы (.@-_)'
}

export const dateFormat = {
    rule: /[0-9.]/g,
    text: 'Формат даты: хх.хх.xxxx',
    isValidate: v => String(v).replace(/[^0-9]/g, '').split('').length === 8,
    mask: v => mask(String(v).replace(/[^0-9]/g, '').split(''), 'хх.хх.xxxx')
}

export const phone = {
    rule: /[0-9+()-]/g,
    text: 'Формат телефона: +x(xxx)xxx-xx-xx',
    isValidate: phone => String(phone).replace(/[^0-9]/g, '').split('').length === 11,
    mask: phone => mask(String(phone).replace(/[^0-9]/g, '').split(''), '+x(xxx)xxx-xx-xx')
}

export const site = {
    rule: /[A-z:/,-_0-9]/g,
    text: 'Допускаются латинские буквы, цифры и символы (:/,-_)'
}

export const regNumber = {
    rule: /[A-Z]/g
}

export const fullName = {
    rule: /[А-я-]/g,
    text: 'Допускаются русские буквы и тире без пробелов'
}

export const countryCity = {
    rule: /[А-я/s-]/g,
    text: 'Допускаются только русские буквы и тире, и пробелы'
}

export const cityCode = {
    rule: /[0-9-]/g,
    text: 'Допускаются только цифры и тире'
}