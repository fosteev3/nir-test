export const SET_FORM = 'SET_FORM';
export const RESET_FORM = 'RESET_FORM';

export function setFormState(state) {
    return {
        type: SET_FORM,
        data: state
    }
}