import { SET_FORM, RESET_FORM } from "../actions/form";

const initialState = {
    certificates: []
};

export default function snackbar(state = initialState, action) {
    switch (action.type) {
        case SET_FORM:
            return {
                ...state,
                ...action.data
            }
        case RESET_FORM: {
            return initialState
        }
        default:
            return state;
    }
}
