import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Collapse } from "@material-ui/core";
import ArrowUp from '../../images/keyboard_arrow_up.svg';
import clsx from "clsx";
import PropTypes from 'prop-types';

const useStyles = makeStyles(() => ({
    root: {
    },
    header: {
        fontSize: 20,
        fontWeight: 'bold',
        cursor: 'pointer',
        userSelect: 'none'
    },
    arrowIcon: {
        marginLeft: 10,
        transition: '0.3s'
    },
    arrowExpand: {
        transform: 'rotate(180deg)',
    }
}));

ExpandFormFields.defaultProps = {
    header: ''
}

ExpandFormFields.propTypes = {
    children: PropTypes.node,
    header: PropTypes.string
}

export default function ExpandFormFields({ children, header }) {
    const classes = useStyles();
    const [expand, setExpand] = useState(true);

    const handleClickHeader = () => setExpand(!expand);

    return (
        <div className={ classes.root }>
            {
                header && (
                    <span className={ classes.header } onClick={ handleClickHeader }>
                        { header }

                        <img className={ clsx(classes.arrowIcon, expand ? classes.arrowExpand : '') }
                             src={ ArrowUp }
                             alt="Arrow up"/>
                    </span>

                )
            }

            <Collapse in={ expand }>
                <div>
                    { children }
                </div>
            </Collapse>
        </div>
    )
}