import React, { useRef } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Form, FormControl } from "react-bootstrap";
import PropTypes from 'prop-types';
import clsx from "clsx";
import { Fade } from "@material-ui/core";

const useStyles = makeStyles({
    root: {},
    container: {
        display: 'flex',
    },
    formControl: {
        flex: 1
    },
    helperText: {
        flex: 1,
        marginTop: 0,
        marginLeft: 15,
        display: 'flex',
        alignItems: 'center'
    },
    label: {
        fontSize: 14
    },
    errorColor: {
        color: "#ff6262",
    }
})

TextField.defaultProps = {
    type: 'text',
    error: false
}

TextField.propTypes = {
    label: PropTypes.string,
    type: PropTypes.oneOf(['text', 'password', 'email', 'number']),
    placeholder: PropTypes.string,
    helperText: PropTypes.string,
    error: PropTypes.bool,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    onChange: PropTypes.func
}

export default function TextField({ label, type, placeholder, helperText, error, onChange, value }) {
    const classes = useStyles();

    const handleChange = (e) => {
        if (onChange) {
            onChange(e);
        }
    }

    const ref = useRef();

    return (
        <Form.Group>
            <Form.Label className={ clsx(classes.label, error && classes.errorColor) }>
                { label }
            </Form.Label>

            <div className={ classes.container }>

                <FormControl className={ classes.formControl }
                              type={ type }
                              value={ value }
                              onChange={ handleChange }
                              placeholder={ placeholder }/>

                <Fade in={error}>
                    <Form.Text className={ clsx(classes.helperText, classes.errorColor) }>
                        { helperText }
                    </Form.Text>
                </Fade>

            </div>
        </Form.Group>
    )
}