import React from "react";

export default function FormValidation({ onSubmit, children }) {

    const handleSubmit = (e) => {
        e.preventDefault();

        const data = {
            values: {},
            errors: []
        }

        const searchValidChildren = children => React.Children.forEach(children, ({ props, type, ...other }) => {
            if (!props) {
                return;
            }

            if (type.name === 'TextField') {
                const { value, validation, name } = props;

                data.values[name] = value || '';

                if ((typeof value === 'undefined') || (value === '')) {
                    data.errors.push({
                        name,
                        isError: true,
                        other: other
                    })
                }

                if (validation && name && (value !== null)) {
                    let isError = String(value).replace(validation.rule, '').length !== 0;

                    if (!isError && validation.isValidate) {
                        isError = !validation.isValidate(value);
                    }

                    data.errors.push({ name, isError });
                } else {
                    console.warn('Cannot validate field');
                }
            } else if (props.children) {
                searchValidChildren(props.children);
            }
        })

        searchValidChildren(children);

        onSubmit(data);
    }

    return (
        <form onSubmit={ handleSubmit }>
            { children }
        </form>
    )
}