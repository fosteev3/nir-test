import React, { useEffect, useState } from "react";
import { Form, FormControl, Button } from "react-bootstrap";
import clsx from "clsx";
import { Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import PropTypes from 'prop-types';
import CrossIcon from '../../images/cancel.svg';

SelectedField.defaultProps = {
    label: '',
    selected: []
}

SelectedField.propTypes = {
    label: PropTypes.string,
    error: PropTypes.bool,
    placeholder: PropTypes.string,
    helperText: PropTypes.string,
    selected: PropTypes.array
}

const useStyles = makeStyles({
    root: {},
    container: {
        display: 'flex',
    },
    formControl: {
        width: 200
    },
    helperText: {

        marginTop: 0,
        marginLeft: 15,
        display: 'flex',
        alignItems: 'center'
    },
    label: {
        fontSize: 14
    },
    errorColor: {
        color: "#ff6262",
    },
    addBtn: {
        marginLeft: 15
    },
    selectedValues: {
        marginTop: 10
    },
    selectedField: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        background: '#71bbff',
        color: '#0500e0',
        padding: 5,
        width: 100,
        borderRadius: 3
    },
    deleteBtn: {
        cursor: 'pointer'
    }
});

export default function SelectedField({ label, placeholder, error, helperText, selected, onChange }) {
    const classes = useStyles();
    const [value, setValue] = useState('');

    const handleChange = (e) => {
        setValue(e.target.value);
    }

    const handleAddBtn = () => {
        if (onChange) {
            onChange(selected.concat(value))
        }

        setValue('');
    }

    const handleDeleteBtn = (v) => {
        if (onChange) {
            onChange(selected.filter(s => s !== v))
        }
    }

    return (
        <Form.Group>
            <Form.Label className={ clsx(classes.label, error && classes.errorColor) }>
                { label }
            </Form.Label>

            <div className={ classes.container }>
                <FormControl className={ classes.formControl }
                             type={ 'text' }
                             value={ value }
                             onChange={ handleChange }
                             placeholder={ placeholder }/>

                {
                    error && (
                        <Form.Text className={ clsx(classes.helperText, classes.errorColor) }>
                            { helperText }
                        </Form.Text>
                    )
                }

                {
                    !error && (
                        <Button className={ classes.addBtn }
                                onClick={ handleAddBtn }
                                disabled={ !value || selected.includes(value) }>
                            Добавить
                        </Button>
                    )
                }

            </div>

            <Grid container className={classes.selectedValues} spacing={2}>
                {
                    selected.map(v => (
                        <Grid key={v} item>
                            <div className={classes.selectedField}>
                                <span>{v}</span>
                                <img className={classes.deleteBtn}
                                     height={10}
                                     width={10}
                                     onClick={e => handleDeleteBtn(v)}
                                     src={CrossIcon} alt="delete btn"/>
                            </div>
                        </Grid>
                    ))
                }
            </Grid>

        </Form.Group>

    )
}