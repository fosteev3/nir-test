import React, { Component } from 'react'
import './App.css';
import { Provider } from 'react-redux';
import { store } from "./utils/store";
import Screens from "./screens";
import 'bootstrap/dist/css/bootstrap.min.css';

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Screens/>
            </Provider>
        );
    }

}

export default App;
